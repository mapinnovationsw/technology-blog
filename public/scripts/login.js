$('form').on('submit', (event) => {
    event.preventDefault();
    axios.post('/login', {
        email: $('#login').val(),
        senha: $('#senha').val()
    })
    .then(response => {
        M.toast({html: response.data, displayLength: 2000, classes: 'rounded'});
        setTimeout( () => window.location.href = '/cms/noticias', 1000);
    })
    .catch(err => M.toast({html: 'Login e/ou senha incorretos. Tente novamente!' + err, displayLength: 2000, classes: 'rounded'}));
});