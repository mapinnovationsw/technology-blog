var onresize = function (e) {
    width = e.target.innerWidth;
    if (width > 992) {
        $('.banner-product').css('top', '-70px');
        $('.banner-product').css('margin-bottom', '-70px');
    }
};

window.addEventListener('resize', onresize);