$('.form-contact').on('submit', e => {
    e.preventDefault();
    axios({
        method: 'POST',
        url: '/mail/',
        data: {
            nome: $('input[name="nome"]').val(),
            email: $('input[name="email"]').val(),
            assunto: $('input[name="assunto"]').val(),
            telefone: $('input[name="telefone"]').val(),
            mensagem: $('textarea[name="mensagem"]').val()
        },
        resposeType: 'json'
    })
    .then(response => {
        if(response.status == 200) {
            $('input[name="nome"]').val('');
            $('input[name="email"]').val('');
            $('input[name="assunto"]').val('');
            $('input[name="telefone"]').val('');
            $('textarea[name="mensagem"]').val('');
            alert('E-mail enviado com sucesso!');
        } else
            alert('Falha no envio do e-mail, tente novamente mais tarde.');
    })
    .catch(error => console.log(error));
});