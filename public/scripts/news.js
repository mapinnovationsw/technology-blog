var onresize = function (e) {
    width = e.target.innerWidth;
    if (width > 992) {
        $('.banner-product').css('top', '-70px');
        $('.banner-product').css('margin-bottom', '-70px');
    }
};

window.addEventListener('resize', onresize);

$(document).ready(function () {
    var titles = $('.news-content .news-title');
    var abstracts = $('.news-content .news-abstract');

    for (let i = 0; i < abstracts.length; i++) {
        $clamp(abstracts[i], {
            clamp: 5
        });
    }
    for (let i = 0; i < titles.length; i++) {
        $clamp(titles[i], {
            clamp: 2
        });
    }
});