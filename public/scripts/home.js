$(document).ready(function () {

    var pathname = window.location.pathname.replace(/\//g, "");
    var solutionOption = 0;

    /*
     * SOLUTIONS SECTION
     */
    $('.solution-option').on('click', function () {
        $('.solution-option').removeClass('active');
        $(this).addClass('active');
        if ($(this).parent().index() != solutionOption) {
            $('.solution').css('display', 'none');
            $($('.solution')[$(this).parent().index()]).fadeIn(300);
        }
        solutionOption = $(this).parent().index();
    });

    $('#select-solutions').on('change', function () {
        $('.solution').css('display', 'none');
        $($('.solution')[this.value]).fadeIn(300);
    });
    /*
     * END SOLUTIONS SECTION
     */

    /*
     *  PARTNERS SECTION
     */
    $('#partners-complete-list-button a').on('click', function () {
        if (!$('.partners').is(':visible')) {
            $('.partners').slideToggle(300);
            setTimeout(() => {
                switch (pathname) {
                    case '':
                        $(this).html('Ocultar lista');
                        break;
                    case 'en-us':
                        $(this).html('Hide list');
                        break;
                    case 'es-es':
                        $(this).html('Ocultar lista');
                        break;
                    default:
                        break;
                }
            }, 300);
            $('html, body').animate({
                scrollTop: 2640
            }, 300);
        } else {
            $('.partners').slideToggle(300);
            setTimeout(() => {
                switch (pathname) {
                    case '':
                        $(this).html('Ver lista completa');
                        break;
                    case 'en-us':
                        $(this).html('See full list');
                        break;
                    case 'es-es':
                        $(this).html('Ver lista completa');
                        break;
                    default:
                        break;
                }
            }, 300);
        }
    });
    /*
     *  END PARTNERS SECTION
     */

    /*
     * NEWS SECTION
     */

    var module = $('.news-content p');
    var titles = $('.news-content .news-title');
    var abstracts = $('.news-content .news-abstract');

    for (let i = 0; i < module.length; i++) {
        $clamp(module[i], {
            clamp: 4
        });
    }

    for (let i = 0; i < titles.length; i++) {
        let divHeight = titles[i].offsetHeight;
        let lineHeight = parseInt($(titles[i]).css('line-height'));
        let lines = divHeight / lineHeight;
        let numberLines = Math.round(lines);
        switch (numberLines) {
            case 1:
                $(titles[i]).css('margin-bottom', '133.6px');
                break;
            case 2:
                $(titles[i]).css('margin-bottom', '102.4px');
                break;

            case 3:
                $(titles[i]).css('margin-bottom', '71.2px');
                break;

            case 4:
                $(titles[i]).css('margin-bottom', '40px');
                break;

            default:
                break;
        }
    }

    for (let i = 0; i < abstracts.length; i++) {
        let divHeight = abstracts[i].offsetHeight;
        let lineHeight = parseInt($(abstracts[i]).css('line-height'));
        let lines = divHeight / lineHeight;
        let numberLines = Math.round(lines);
        switch (numberLines) {
            case 1:
                $(abstracts[i]).css('margin-bottom', '112px');
                break;
            case 2:
                $(abstracts[i]).css('margin-bottom', '88px');
                break;

            case 3:
                $(abstracts[i]).css('margin-bottom', '64px');
                break;

            case 4:
                $(abstracts[i]).css('margin-bottom', '40px');
                break;

            default:
                break;
        }
    }

    /*
     * END NEWS SECTION
     */

    /*
     * RESIZE CONTROL
     */
    var onresize = function (e) {
        for (let i = 0; i < titles.length; i++) {
            let divHeight = titles[i].offsetHeight;
            let lineHeight = parseInt($(titles[i]).css('line-height'));
            let lines = divHeight / lineHeight;
            let numberLines = Math.round(lines);
            switch (numberLines) {
                case 1:
                    $(titles[i]).css('margin-bottom', '133.6px');
                    break;
                case 2:
                    $(titles[i]).css('margin-bottom', '102.4px');
                    break;

                case 3:
                    $(titles[i]).css('margin-bottom', '71.2px');
                    break;

                case 4:
                    $(titles[i]).css('margin-bottom', '40px');
                    break;

                default:
                    break;
            }
        }

        for (let i = 0; i < abstracts.length; i++) {
            let divHeight = abstracts[i].offsetHeight;
            let lineHeight = parseInt($(abstracts[i]).css('line-height'));
            let lines = divHeight / lineHeight;
            let numberLines = Math.round(lines);
            switch (numberLines) {
                case 1:
                    $(abstracts[i]).css('margin-bottom', '112px');
                    break;
                case 2:
                    $(abstracts[i]).css('margin-bottom', '88px');
                    break;

                case 3:
                    $(abstracts[i]).css('margin-bottom', '64px');
                    break;

                case 4:
                    $(abstracts[i]).css('margin-bottom', '40px');
                    break;

                default:
                    break;
            }
        }
    };

    window.addEventListener('resize', onresize);
    /*
     * END RESIZE CONTROL
     */

});