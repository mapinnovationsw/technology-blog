function deleteNoticia(id) {
    const c = confirm(`Deseja excluir essa notícia?`);
    if (c == true) {
        axios.delete(`http://blog.maptechnology.com.br/api/noticias`, {
            data: {
                id: id
            }
        }).then(response => {
            alert(response.data);
            location.reload();
        });
    }
}