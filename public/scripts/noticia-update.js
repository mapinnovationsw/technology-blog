function updateNoticia(id) {
    axios.put(`/api/noticias`, {
        data: {
            id: id
        }
    }).then(response => {
        alert(response.data);
        location.reload();
    });
}