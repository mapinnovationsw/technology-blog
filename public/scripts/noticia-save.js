var cloudinaryURL = 'https://api.cloudinary.com/v1_1/mapinnovation/upload',
    cloudinaryPRESET = 'qngt0dvn',
    imagem;

$(() => $('textarea').froalaEditor());

$('input[name=imagem]').on('change', (event) => {
    const file = event.target.files[0];
    const formData = new FormData();

    formData.append('file', file);
    formData.append('upload_preset', cloudinaryPRESET);
    axios({
        url: cloudinaryURL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: formData
    })
    .then(response => {
        imagem = response.data.secure_url;
        $('.btn').removeClass('hide');
    })
    .catch(error => console.log(error));
});


$('#send').on('click', () => {
    
    axios.post(`http://blog.maptechnology.com.br/api/noticias`, {
        corpo: $('textarea').val(),
        resumo: $('textarea').val().replace(/(<([^>]+)>)/ig, ""),
        titulo: $('input[name=titulo]').val(),
        categoria: $('select[name=categoria]').val(),
        data: $('input[name=data]').val(),
        imagem: imagem
    }).then(response => {
        alert('Matéria adicionada com sucesso!');
        window.location = '/cms/noticias';
    });
});