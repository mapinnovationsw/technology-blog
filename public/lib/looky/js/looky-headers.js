var header = document.getElementsByClassName('header-fixed-with-scroll')[0];
var sticky = header.offsetTop;

var onresize = function (e) {
    width = e.target.innerWidth;
    $('#nav-icon').removeClass('open');
    $('.navigation').removeClass('mobile');
    if (width > 992) {
        $('.navigation').css('display', 'block');
    } else {
        $('.navigation').css('display', 'none');
    }
};

window.addEventListener('resize', onresize);

// MENU BUTTON CLICK
$('#nav-icon').click(function () {
    $(this).toggleClass('open');
    $('.navigation').addClass('mobile');
    $('.navigation').toggle(300);
    if ($(this).hasClass('open'))
        $('.search-bar-input').val('');
});
// END MENU BUTTON CLICK

// FIXED HEADER
$(window).scroll(function () {
    headerToFixed();
});

function headerToFixed() {
    if ($(window).scrollTop() > sticky) {
        $(header).addClass('sticky');
        $(header).css('background-color', '#0e294d');
        if (window.innerWidth > 992) {
            $('.slider').css('top', 'auto');
            $('.slider').css('margin-bottom', 'auto');
            $('.banner-product').css('top', 'auto');
            $('.banner-product').css('margin-bottom', 'auto');
        }
    } else {
        $(header).removeClass('sticky');
        $(header).css('background-color', '');
        if (window.innerWidth > 992) {
            $('.slider').css('top', '-70px');
            $('.slider').css('margin-bottom', '-70px');
            $('.banner-product').css('top', '-70px');
            $('.banner-product').css('margin-bottom', '-70px');
        }
    }
}
// END FIXED HEADER

// SEARCHBAR
$('.search-bar-img').on('click', function () {
    if (!$(this).parent().hasClass('active')) {
        $('.search-bar').find('input').animate({
            width: 'toggle'
        }, 300);
        $(this).parent().addClass('active');
        $(this).parent().find('input').focus();
    } else {
        console.log($(this).parent().find('input').val());
        $(this).parent().find('input').val('');
        $(this).parent().find('input').focus();
    }
});

$(document).mouseup(function (e) {
    if (!$('.search-bar').is(e.target) && $('.search-bar').has(e.target).length === 0) {
        $('.search-bar').find('input').animate({
            width: 'hide'
        }, 300);
        $('.search-bar').removeClass('active');
        $('.search-bar').find('input').val('');
    }
});
// END SEARCHBAR

// TOPBAR
$('.language').on('click', function () {
    var myLocation = window.location.pathname;
    if ($(this).hasClass('pt-br'))
        verifyLanguage('pt-br', myLocation);

    if ($(this).hasClass('en-us'))
        verifyLanguage('/en-us/', myLocation);

    if ($(this).hasClass('es-es'))
        verifyLanguage('/es-es/', myLocation);
});
// END TOPBAR

// LANGUAGE CONTROL
function verifyLanguage(language, path) {
    var newPath;

    language == 'pt-br' ? language = '/' : language = language;
    if (path.includes('/en-us/') || path.includes('/es-es/'))
        newPath = path.replace(/\/en-us\/|\/es-es\//, language);
    else
        newPath = language + path;

    newPath = newPath.replace('//', '/');
    window.location.href = newPath;
}
// END LANGUAGE CONTROL

// MENU CONTROL
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
// END MENU CONTROL