var onresize = function (e) {
    width = e.target.innerWidth;
    if (width > 992) {
        $('.slider').css('top', '-70px');
        $('.slider').css('margin-bottom', '-70px');
    }
};

window.addEventListener('resize', onresize);

var intervalSlider;
var slideIndex = 2;

$('.ctrl').on('click', function () {
    currentSlide($(this).index() + 1);
});

intervalSlider = setInterval(autoSlide, 8000);

function currentSlide(n) {
    showSlides(slideIndex = n);
    clearInterval(intervalSlider);
    intervalSlider = setInterval(autoSlide, 8000);
}

function autoSlide() {
    showSlides(slideIndex);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName('item');
    var ctrls = document.getElementsByClassName('ctrl');
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none';
    }
    for (i = 0; i < ctrls.length; i++) {
        ctrls[i].className = ctrls[i].className.replace(' active', '');
    }
    slides[slideIndex - 1].style.display = 'block';
    ctrls[slideIndex - 1].className += ' active';
    slideIndex++;
}