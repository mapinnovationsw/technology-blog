function mascarar(src, tipo, e) {
    switch (tipo.toLowerCase()) {
        case 'moeda':
            //-- alinhando a direita
            src.style.textAlign = '';
            mascara(src, moeda);
            break;
        case 'data':
        case '##/##/####':
            mascara(src, data);
            break;
        case 'hora':
        case '##:##':
            mascara(src, hora);
            break;
        case 'cpf':
        case '###.###.###-##':
            mascara(src, cpf);
            break;
        case 'cnpj':
        case '##.###.###/####-##':
            mascara(src, cnpj);
            break;
        case 'cep':
        case '#####-###':
            mascara(src, cep);
            break;
        case 'celular':
        case 'telefone':
        case '(##) ####-####':
        case '##-####-####':
        case '(##) #####-####':
        case '##-#####-####':
            src.title = 'Este campo suporta o 9º dígito para telefone';
            mascara(src, fone);
            break;
        case 'placa':
        case '###-####':
            mascara(src, placa);
            break;
        case 'inteiro':
        case 'numero':
        case 'number':
            mascara(src, number);
            break;
        case 'real':
        case 'float':
        case 'valor':
            mascara(src, valor);
            break;
        case 'letra':
        case 'letranumero':
        case 'letraenumero':
        case 'letra e numero':
            mascara(src, letra);
            break;
        default:
            // Qualquer mascara usando ###
            var i = src.value.length;
            var texto = mascara.substring(i);
            var tecla;
            //--- Inicio resolver problema de backspace
            if (e) {
                ie = /msie/i.test(navigator.userAgent);
                if (ie) {
                    tecla = event.keyCode;
                } else {
                    tecla = e.which;
                    if (tecla == 8)
                        src.value = '';
                }
            }
            if (tecla && texto.substring(0, 1) != '#') {
                src.value += texto.substring(0, 1);
            }
            break;
    }
}

// Funcões para mascarar o campo
function mascara(o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout(execmascara(), 1);
}

function execmascara() { // Executa a máscara
    v_obj.value = v_fun(v_obj.value);
}

function cep(v) { // 00.000-000
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d{5})(\d)/, "$1-$2");
    v = v.substring(0, 9);
    // Limite
    return v;
}

function cpf(v) { // 000.000.000-00
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1-$2");
    v = v.substring(0, 14);
    // Limite
    return v;
}

function cnpj(v) { // 00.000.000/0001-00
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1/$2");
    v = v.replace(/(\d{4})(\d)/, "$1-$2");
    v = v.substring(0, 18);
    // Limite
    return v;
}

function data(v) { // 00/00/0000
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1/$2");
    v = v.replace(/(\d{2})(\d)/, "$1/$2");
    v = v.replace(/(\d{4})(\d{1,2})$/, "$1/$2");
    v = v.substring(0, 10);
    // Limite
    return v;
}

function hora(v) { // 00/00/0000
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1:$2");
    v = v.replace(/(\d{2})(\d)/, "$1:$2");
    v = v.substring(0, 5);
    // Limite
    return v;
}

function fone(v) { // 00-0000-0000 00-0-0000-0000
    if (v.length <= 14) {
        v = v.replace(/\D/g, "");
        v = v.replace(/(\d{2})(\d)/, "($1) $2");
        v = v.replace(/(\d{4})(\d)/, "$1-$2");
        v = v.substring(0, 14);
    } else if (v.length >= 15) {
        v = v.replace(/\D/g, "");
        v = v.replace(/(\d{2})(\d)/, "($1) $2");
        v = v.replace(/(\d{5})(\d)/, "$1-$2");
        v = v.substring(0, 15);
    }
    return v;
}

function placa(v) { // 000-0000
    if (v.length <= 8) {
        v = v.toUpperCase();
        v = v.replace(/\W/g, ""); // Remove caracteres caracteres especiais
        v = v.replace(/(\w{3})(\w)/, "$1-$2");
        v = v.substring(0, 8);
    } else if (v.length >= 8) {
        v = v.replace(/\W/g, "");
        v = v.replace(/(\w{3})(\w)/, "$1-$2");
        v = v.substring(0, 8);
    }
    return v;
}

function numero(v) { // 0-9
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    return v;
}

function letra(v) { // a-z
    v = v.replace(/\W/g, ""); // Remove caracteres caracteres especiais
    return v;
}

function valor(v) {
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{2})$/, "$1.$2"); // Coloca ponto antes dos 2 últimos digitos
    return v;
}

function moeda(v) {
    v = v.replace(/\D/g, ""); // Remove tudo o que não é dígito
    v = v.replace(/(\d+)(\d{2})/, "$1,$2"); // Insere a vírgula
    v = v.replace(/(\d+)(\d{3},\d{2})$/g, "$1.$2");
    // Coloca o primeiro ponto
    var qtdLoop = (v.length - 3) / 3;
    var count = 0;
    while (qtdLoop > count) {
        count++;
        v = v.replace(/(\d+)(\d{3}.*)/, "$1.$2");
        // Coloca o resto dos pontos
    }
    //-- Verificando tamanho
    if ('' + v.length > 2) {
        v = v.replace(/^(0+)(\d)/g, "$2");
        // Remove 0 à esquerda
    }
    return v;
}