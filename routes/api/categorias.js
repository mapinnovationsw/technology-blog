const express = require('express'),
    router = express.Router(),
    db = require('../../database');

router
.post('/', (request, response, next) => {
    db.query(`
        INSERT INTO categorias(nome)
        VALUES (?)
    `, [request.body.nome], (error, results) => {
        if(error) response.status(500).send('Erro ao cadastrar categoria. Tente novamente mais tarde. ' + error);

        response.status(200).send('Categoria cadastrada com sucesso!');
    });
})
.get('/', (request, response, next) => {
    db.query(`
        SELECT * FROM categorias
        ORDER BY 
            id
        DESC
    `, (error, results) => {
        if(error) response.status(500).send('Erro ao encontrar categorias. Tente novamente mais tarde. ' + error);

        response.status(200).json(results);
    });
})
.put('/', (request, response, next) => {
    db.query(`
        UPDATE categorias SET nome = ?
        WHERE id = ?
    `, [
        request.body.nome,
        request.body.id
    ], (error, results) => {
        if(error) response.status(500).send(`Erro ao atualizar categorias. Tente novamente mais tarde. ${error}`);

        response.status(200).send('Categoria atualizada com sucesso!');
    });
})
.delete('/', (request, response, next) => {
    db.query(`
        DELETE FROM categorias WHERE id = ?
    `, [request.body.id], (error, results) => {
        if(error) response.status(500).send(`Erro ao excluir categoria. Tente novamente mais tarde. ${error}`);

        response.status(200).send('Categoria excluída com sucesso!');
    });
});

module.exports = router;