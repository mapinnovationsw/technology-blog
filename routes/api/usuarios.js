const express = require('express'),
    db = require('../../database'),
    slug = require('slug'),
    router = express.Router(),
    api = require('../../helpers/api');
    
router
.post('/', api.hasToken, api.isAdm, (request, response, next) => {
    db.query(`
        INSERT INTO usuario (nome, email, perfil, senha)
        VALUES (?, ?, ?, ?)
    `, [
        request.body.nome,
        request.body.email,
        request.body.perfil,
        request.body.senha
    ], (error, results) => {
        if(error) response.status(500).send(`Erro ao cadastrar usuário. Tente novamente mais tarde. ${error}`);

        response.status(200).send('Usuário cadastrado com sucesso!');
    });
})
.get('/', (request, response, next) => {
    db.query(`
        SELECT * FROM usuario
    `, (error, results) => {
        if(error) response.status(500).send(`Erro ao buscar usuários. Tente novamente mais tarde. ${error}`);

        response.status(200).json({usuarios: results});
    });
})
.put('/', api.hasToken, api.isAdm, (request, response, next) => {
    db.query(`
        UPDATE usuario
        SET perfil = ?
        WHERE usuario.id = ?
    `, [
        request.body.perfil,
        request.body.id
    ], (error, restults) => {
        if(error) response.status(500).send(`Erro ao alterar usuário. Tente novamente mais tarde. ${error}`);

        response.status(200).send('Usuário alterado com sucesso.');
    });
})
.delete('/', api.hasToken, api.isAdm, (request, response, next) => {
    db.query(`
        DELETE FROM usuario WHERE usuario.id = ?
    `, [request.body.id], (error, results) => {
        if (error) response.status(500).send(`Erro ao excluir usuário. Tente novamente mais tarde. ${error}`);

        if(results.affectedRows != 0)
            response.status(200).send('Usuário excluído com sucesso.');
        else
            response.status(200).send('Id de usuário não existe');
    });
});


module.exports = router;