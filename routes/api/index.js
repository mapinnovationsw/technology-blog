const noticias = require('./noticias'),
    categorias = require('./categorias'),
    usuarios = require('./usuarios');

module.exports = (app) => {
    app
    .use('/api/categorias', categorias)
    .use('/api/noticias', noticias)
    .use('/api/usuarios', usuarios);
};