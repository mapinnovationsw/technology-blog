const express = require('express'),
    db = require('../../database'),
    slug = require('slug'),
    router = express.Router(),
    api = require('../../helpers/api');

router
.post('/', (request, response, next) => {
    db.query(`
        INSERT INTO noticias(corpo, resumo, titulo, slug, id_categoria, data, imagem)
        VALUES (?, ?, ?, ?, ?, ?, ?)
    `, [
        request.body.corpo,
        request.body.resumo,
        request.body.titulo,
        slug(request.body.titulo, {
            lower: true
        }),
        request.body.categoria,
        request.body.data,
        request.body.imagem
    ], (error, results) => {
        if (error) response.status(500).send('Erro ao inserir notícia. Tente novamente mais tarde. ' + error);

        response.status(200).send('Notícia cadastrada com sucesso!');
    });
})
.get('/', (request, response, next) => {
    db.query(`
        SELECT
            noticias.*,
            DATE_FORMAT(noticias.data, "%d/%m/%Y") AS data,
            categorias.nome AS categoria
        FROM noticias
        RIGHT JOIN categorias ON categorias.id = noticias.id_categoria
        ORDER BY
            noticias.data
        DESC
    `, (error, results) => {
        if(error) response.status(500).send('Erro ao buscar notícias. Tente novamente mais tarde. ' + error);

        response.status(200).json(results);
    });
})
.put('/', (request, response, next) => {
    response.status(200).json(request);
})
.delete('/', (request, response, next) => {
    db.query(`
        DELETE FROM noticias WHERE id = ?
    `, [
        request.body.id
    ], (error, results) => {
        if (error) response.status(500).send('Erro ao excluir notícia. Tente novamente mais tarde. ' + error);

        response.status(200).send('Notícia excluída com sucesso!');
    });
})
.get('/principais', (request, response, next) => {
        db.query(`
        SELECT
            noticias.*,
            DATE_FORMAT(noticias.data, "%d/%m/%Y") AS data,
            categorias.nome AS categoria
        FROM noticias
        RIGHT JOIN categorias ON categorias.id = noticias.id_categoria
        ORDER BY
            noticias.data
        DESC
        LIMIT 3
    `, (error, results) => {
            if (error) response.status(500).send('Erro ao buscar notícias. Tente novamente mais tarde. ' + error);

            response.status(200).json({
                noticias: results
            });
        });
    })
    .get('/:slug', (request, response, next) => {
        db.query(`
        SELECT
            noticias.*,
            DATE_FORMAT(noticias.data, "%W, %d/%m/%Y") AS data,
            categorias.nome AS categoria
        FROM noticias
        RIGHT JOIN categorias ON categorias.id = noticias.id_categoria
        WHERE noticias.slug LIKE ?
        ORDER BY
            data
        DESC
    `, [request.params.slug], (error, results) => {
            if (error) response.status(500).send('Não foi possível achar a notícia. Tente novamente mais tarde! ' + error);

            response.status(200).json(results[0]);
        });
    });

module.exports = router;