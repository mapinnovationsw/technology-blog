const express = require('express'),
    axios = require('axios'),
    auth = require('../helpers/auth'),
    router = express.Router();

router
.get('/', (request, response, next) => {
    axios({
        method: 'GET',
        url: `${process.env.API_URL}/noticias`
    })
    .then(noticias => response.render('noticias', {title: 'Notícias', noticias: noticias.data, back: 'http://maptechnology.com.br/'}))
    .catch(error => response.json(error));
})
.get('/noticias/cadastrar', auth.isLogged, (request, response, next) => {
    axios({
        method: 'GET',
        url: `${process.env.API_URL}/categorias`
    })
    .then(categorias => response.render('noticias-form', {title: 'Map Technology | CMS - Cadastrar Notícia', categorias: categorias.data, back: '/'}))
    .catch(error => response.json(error));
})
.get('/noticias/:slug', (request, response, next) => {
    axios({
        method: 'GET',
        url: `${process.env.API_URL}/noticias/${request.params.slug}`
    })
    .then(noticia => response.render('noticia', {title: noticia.data.titulo, noticia: noticia.data, back: '/'}))
    .catch(error => response.json(error));
});

module.exports = router;