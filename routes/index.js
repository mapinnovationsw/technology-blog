const noticias = require('./noticias'),
    cms = require('./cms'),
    login = require('./login');

module.exports = (app) => {
    app
    .use('/', noticias)
    .use('/login', login)
    .use('/cms', cms);
};