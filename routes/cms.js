const express = require('express'),
    axios = require('axios'),
    router = express.Router(),
    auth = require('../helpers/auth'),
    app = require('../helpers/app');

router
.get('/noticias', auth.isLogged, (request, response, next) => {
    axios.get(`${process.env.API_URL}/noticias`)
    .then(noticias => response.render('noticias/index', { title: 'Map Technology | CMS - Lista', noticias: noticias.data, back: '/cms', user: request.session.user.nome}))
    .catch(error => response.json(error));
})
.get('/categorias', (request, response, next) => {
    response.status(200).render('categoria/index', {back: '', title: ''});
})
.get('/usuarios', auth.isLogged, app.isAdm, (request, response, next) => {
    response.status(200).send('usuários');
});

module.exports = router;