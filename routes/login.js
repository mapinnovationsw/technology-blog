const express = require('express'),
    jwt = require('jsonwebtoken'),
    router = express.Router(),
    auth = require('../helpers/auth'),
    db = require('../database/index');

router
.post('/', (request, response, next) => {
    db.query(`
        SELECT * FROM usuario
        WHERE
            usuario.email LIKE ? AND
            usuario.senha LIKE ?
    `, [
        request.body.email,
        request.body.senha
    ], (error, results) => {
        if(error) response.status(500).send(`Erro ao tentar login. Tente novamente mais tarde. ${error}`);

        if(results.length === 0) response.status(401).send(`Login e/ou senha incorretos.`);
        else {
            jwt.sign({user: results}, process.env.JWT_SECRET, {expiresIn: '24h'}, (err, token) => {
                if(err) response.status(500).send(err);

                request.session.token = token;
                request.session.perfil = results[0].perfil;
                request.session.user = results[0];

                response.status(200).send('Logado com sucesso!');
            });
        }
    });
})
.post('/verify', (request, response, next) => {
    if(request.headers['authorization'] !== undefined) {
        jwt.verify(request.headers['authorization'].split(' ')[1], process.env.JWT_SECRET, (err, auth) => {
            if(err) response.status(403).send('Token inválido.');

            response.status(200).json({
                auth,
                token: {
                    gerado: new Date(auth.iat*1000).toLocaleString('pt-br', {timezone: 'Brazil/brt'}),
                    expira: new Date(auth.exp*1000).toLocaleString('pt-br', {timezone: 'Brazil/brt'})
                }
            });
        });
    }
})
.get('/logout', auth.isLogged, (request, response, next) => {
    request.session.destroy();
    response.redirect('/login');
})
.get('/', auth.loginBlock, (request, response, next) => {
    response.render('login', { title: 'Map Technology | CSM - Login', back: '/'});
});

module.exports = router;