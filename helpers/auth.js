const jwt = require('jsonwebtoken');

module.exports = {
    isLogged: (request, response, next) => {
        if (request.session.token) {
            jwt.verify(request.session.token, process.env.JWT_SECRET, (err, auth) => {
                if(err) {
                    request.session.destroy();
                    response.redirect('/login');
                }
                return next();
            });
        } else
            response.redirect('/login');
    },
    loginBlock: (request, response, next) => {
        if(request.session.token)
            return response.redirect('/cms');
        else
            return next();
    }
};