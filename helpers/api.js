const jwt = require('jsonwebtoken');

module.exports = {
    hasToken: (request, response, next) => {
        let hasToken;
        request.headers['authorization'] !== undefined ? hasToken = request.headers['authorization'].split(' ')[1] : hasToken = request.session.token;
        if (hasToken !== undefined) {
            jwt.verify(hasToken, process.env.JWT_SECRET, (err, auth) => {
                if (err) response.status(403).send('Token inválido.');

                return next();
            });
        } else
            response.status(403).send('Token inválido.');
    },
    isAdm: (request, response, next) => {
        let token;
        request.headers['authorization'] !== undefined ? token = request.headers['authorization'].split(' ')[1] : token = request.session.token;

        token = jwt.decode(token, {
            complete: true
        });
        if (token.payload.user[0].perfil == 0) return next();
        else return response.status(403).send('Você não tem permissão para fazer essa operação.');
    }
};