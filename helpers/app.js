module.exports = {
    isAdm: (request, response, next) => {
        if(request.session.perfil == 0)
            return next();
        else
            return response.redirect('/cms');
    }
};